# abe
## Arch Linux® installation process made (sort of) easy.
### Collection of BASH scripts to guide through Arch Linux® installation in simple and productive manner.

**CONSIDERATIONS** project status is 'work very much in progress', development version; not production ready by any means, lots more work needs doing

**LIMITATIONS**
- single partition install only
- root partition formatting option limited to 'ext4'
- no partitioning functionalities (prepare partitions prior to installing)
- bootloader installation not implemented as of yet

**NECESSITIES** Arch environment and 'arch-install-scripts', network access and root privileges

**PURPOSE** ultimately deployed to live *Archiso* environment to ease your disease

**USAGE** `./abe`
